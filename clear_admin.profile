<?php
// $Id$
/**
 * @file
 *
 *
 *
 * @author Kálmán Hosszu - hosszu.kalman@gmail.com - http://www.kalman-hosszu.com
 */

/* ====================== */
/* ==== DRUPAL HOOKS ==== */
/* ====================== */

/**
 * Implementation of hook_profile_details().
 */
function clear_admin_profile_details() {
  return array(
    'name' => 'Clear admin',
    'description' => 'More clear administration user interface for Drupal.',
  );
}

/**
 * Implementation of hook_profile_modules().
 */
function clear_admin_profile_modules() {
  return array(
    // Enable core modules.
    'user', 'filter', 'block', 'node', 'taxonomy', 'comment', 'profile', 'menu', 'dblog',
    // Enable contirb modules.
    'admin_menu', 'module_filter', 'hierarchical_select', 'hs_flatlist', 'hs_menu', 'hs_smallhierarchy', 'hs_taxonomy', 'vertical_tabs',
  );
}

/**
 * Implementation of hook_profile_tasks().
 */
function clear_admin_profile_tasks(&$task, $url) {
  if ($task == 'profile') {
    // Enable admin themes
    db_query("UPDATE {system} SET status = 1 WHERE type = 'theme' and name IN ('%s', '%s')", 'cube', 'tao');
    // and set admin theme
    variable_set('admin_theme', 'cube');
    // Perform the default profile install tasks.
    include_once('profiles/default/default.profile');
    default_profile_tasks($task, $url);
  }
}

/**
 * Implementation of hook_form_alter().
 */
function clear_admin_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'install_configure') {
    // Set default for site name field.
    $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
  }
}
