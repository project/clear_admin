
-- SUMMARY --

This profile creates more clear administration user interface with Rubik theme and the following modules:

  * Administration menu
  * Hierarchical Select
  * Hierarchical Select Flat List
  * Hierarchical Select Menu
  * Hierarchical Select Small Hierarchy
  * Hierarchical Select Taxonomy
  * Vertical Tabs
