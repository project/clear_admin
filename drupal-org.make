core = 6.x
api = 2

; Modules
projects[admin_menu][subdir] = contrib
projects[admin_menu][version] = 1.8
projects[module_filter][subdir] = contrib
projects[module_filter][version] = 1.7
projects[hierarchical_select][subdir] = contrib
projects[hierarchical_select][version] = 3.8
projects[vertical_tabs][subdir] = contrib
projects[vertical_tabs][version] = 1.0-rc2

; Themes
projects[rubik][subdir] = contrib
projects[rubik][version] = 3.0-beta3
projects[tao][subdir] = contrib
projects[tao][version] = 3.3
